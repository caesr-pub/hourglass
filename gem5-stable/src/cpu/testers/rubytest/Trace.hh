#define TRACE 1
#define WRITE_HITS 0
// BASELINE 1 does not use proposed solution
// BASELINE 0 uses proposed solution
#define NPROC 4
#define NPROC_C 2
#define NPROC_NC 2
#define ALL_CRITICAL 0
// For ALL_CRITICAL set as 1, set NPROC_C equal to NPROC
#define BASELINE 0
#define SLOT_WIDTH 50
// 50 for NPROC = 4; for increase in number of cores, SLOT_WIDTH is increased to allow for all cores to transmit coherence messages in the worst case.
// 60 for NPROC = 8;
#define MAX_NPROC 8
#define SYNTH 1
//Set SYTNH as 1 to run synthetic benchmarks that have different trace files for different cores; Use with NPROC == 4.
//trace-c0.trc, trace-c1.trc, trace-c2.trc, trace-c3.trc are the trace file names for the 4 cores.

